# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('truckmanagement', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='truck',
            name='destination_county',
        ),
        migrations.AddField(
            model_name='truck',
            name='destination_country',
            field=models.CharField(default='NLD', max_length=3, choices=[(b'NLD', b'The Netherlands'), (b'FRA', b'France'), (b'DEU', b'Germany')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='block',
            name='block_number',
            field=models.PositiveIntegerField(max_length=1, choices=[(1, b'Blok 1'), (2, b'Blok 2'), (3, b'Blok 3'), (4, b'Blok 4')]),
        ),
    ]
