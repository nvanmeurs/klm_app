# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Block',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('block_number', models.PositiveIntegerField(max_length=1, choices=[(b'Blok 1', 1), (b'Blok 2', 2), (b'Blok 3', 3), (b'Blok 4', 4)])),
                ('telephone_number', models.CharField(max_length=14)),
                ('tasks_description', models.CharField(max_length=100, blank=True)),
                ('date', models.DateField()),
                ('assigned_employee', models.ForeignKey(related_name=b'assigned blocks', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Truck',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('departure_time', models.TimeField()),
                ('truck_number', models.CharField(max_length=10)),
                ('destination_county', models.CharField(max_length=3, choices=[(b'The Netherlands', b'NLD'), (b'France', b'FRA'), (b'Germany', b'DEU')])),
                ('assigned_block', models.ForeignKey(related_name=b'trucks', to='truckmanagement.Block')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
