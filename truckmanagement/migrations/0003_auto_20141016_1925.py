# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('truckmanagement', '0002_auto_20141016_1904'),
    ]

    operations = [
        migrations.AlterField(
            model_name='block',
            name='assigned_employee',
            field=models.ForeignKey(related_name=b'blocks', blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='block',
            name='block_number',
            field=models.PositiveIntegerField(max_length=1, choices=[(1, b'Block 1'), (2, b'Block 2'), (3, b'Block 3'), (4, b'Block 4')]),
        ),
    ]
