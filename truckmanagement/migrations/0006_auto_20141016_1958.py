# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('truckmanagement', '0005_auto_20141016_1928'),
    ]

    operations = [
        migrations.AlterField(
            model_name='block',
            name='block_number',
            field=models.PositiveIntegerField(max_length=1),
        ),
        migrations.AlterField(
            model_name='truck',
            name='destination_country',
            field=models.CharField(max_length=5),
        ),
    ]
