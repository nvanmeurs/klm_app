from django.contrib import admin
from truckmanagement.models import Truck, Block

admin.site.register(Truck)
admin.site.register(Block)