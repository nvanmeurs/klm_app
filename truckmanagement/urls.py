from django.conf.urls import patterns, include, url
from rest_framework.routers import DefaultRouter
from truckmanagement.views import TruckViewSet, BlockViewSet, UserViewSet

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'trucks', TruckViewSet)
router.register(r'blocks', BlockViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls))
)