from django.contrib.auth.models import User
from rest_framework import viewsets
from truckmanagement.models import Truck, Block
from truckmanagement.serializers import TruckSerializer, BlockSerializer, UserSerializer

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

class BlockViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows blocks to be viewed or edited.
    """
    queryset = Block.objects.all()
    serializer_class = BlockSerializer

class TruckViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows trucks to be viewed or edited.
    """
    queryset = Truck.objects.all()
    serializer_class = TruckSerializer