from django.db import models

class Block(models.Model):
    block_number = models.PositiveIntegerField(max_length=1)
    telephone_number = models.CharField(max_length=14)
    tasks_description = models.CharField(max_length=100, blank=True)
    date = models.DateField()
    assigned_employee = models.ForeignKey('auth.User', related_name='blocks', blank=True, null=True)

    def __unicode__(self):
        return 'Block %d (%s)' % (self.block_number, self.date)

class Truck(models.Model):
    departure_time = models.TimeField()
    truck_number = models.CharField(max_length=10)
    destination_country = models.CharField(max_length=5)
    assigned_block = models.ForeignKey(Block, related_name='trucks')

    def __unicode__(self):
        return '%s %s %s' % (self.departure_time, self.truck_number, self.destination_country)