from django.contrib.auth.models import User
from rest_framework import serializers
from truckmanagement.models import Truck, Block

class TruckSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Truck
        fields = ('url', 'departure_time', 'truck_number', 'destination_country', 'assigned_block')

class BlockSerializer(serializers.HyperlinkedModelSerializer):
    trucks = TruckSerializer(many=True)

    class Meta:
        model = Block
        fields = ('url', 'block_number', 'telephone_number', 'tasks_description', 'date', 'assigned_employee','trucks')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    klm_id = serializers.Field(source='username')
    blocks = BlockSerializer(many=True)

    class Meta:
        model = User
        fields = ('url', 'klm_id', 'first_name', 'last_name', 'blocks')